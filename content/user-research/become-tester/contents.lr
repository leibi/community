section: user research
---
section_id: user-research
---
color: primary
---
_model: page
---
title: Become an alpha tester
---
subtitle: We regularly release Tor Browser early versions to allow users to test software improvements and new ideas. Sign up to be in our testing pool.
---
key: 1
---
html: two-columns-page.html
---
body:

# Become an alpha tester

To help keep Tor Browser available on so many platforms, we rely on a community of volunteer Quality Assurance (QA) testers to trial alpha versions of Tor Browser before its general release. In addition to finding and reporting bugs, alpha testers also have the opportunity to provide feedback on new features to the team before they reach Tor Browser stable.


## How to get started

* Join the [Tor QA mailing list](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-qa) to receive an email whenever a new alpha is ready for testing.

* Sign up for an account on the [Tor Forum](https://forum.torproject.net/) and post any issues you discover in the [Tor Browser Alpha Feedback](https://forum.torproject.net/c/feedback/tor-browser-alpha-feedback/6) category.

* Or, if you have a Gitlab account, post any bugs you find using the templates provided in [applications/tor-browser](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues) (but make sure to search the repo for duplicates first).


Please only [download a Tor Browser Alpha](https://www.torproject.org/download/alpha/) if you are okay with some things not working properly, want to help us find and [report bugs](https://support.torproject.org/misc/bug-or-feedback/), and are not putting yourself at risk.
